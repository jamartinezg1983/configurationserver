package com.kesizo.microservices.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;


/*
 * Example from https://medium.com/@migueldoctor/spring-cloud-series-c%C3%B3mo-crear-y-configurar-microservicios-para-que-sean-accesibles-a-trav%C3%A9s-de-7792c6e68b80
 */
@EnableConfigServer
@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		System.setProperty("spring.jackson.serialization.INDENT_OUTPUT", "true");
		SpringApplication.run(Application.class, args);
	}
}